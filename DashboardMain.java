package com.fora.foodapp.dashboard;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import com.fora.foodapp.BottomNavigationViewHelper;
import com.fora.foodapp.R;
import com.fora.foodapp.dashboard.fragment.AccountFragment;
import com.fora.foodapp.dashboard.fragment.FavouriteFragment;
import com.fora.foodapp.dashboard.fragment.OrderFragment;
import com.fora.foodapp.dashboard.fragment.ScheduleFragment;
import com.fora.foodapp.database.DatabaseAccess;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardMain extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    shoHome();
                    break;
                case R.id.navigation_account:
                    showAccount();
                    break;

                case R.id.navigation_schedule:
                    showSchedule();
                    break;
                case R.id.navigation_fav:
                    showFav();
                    break;
                case R.id.navigation_order:
                    showOrder();
                    break;
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            try {
                if(bundle.getString("DashBord").toString().equalsIgnoreCase("WishList"))
                {
                    showFav();
                    navigation.setSelectedItemId(R.id.navigation_fav);
                }


            } catch (NullPointerException e ) {
                shoHome();
            }




        }
        else {
            shoHome();
        }



    }
    public  void shoHome()
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        DashboardMainFrahment homeFragment=new DashboardMainFrahment();
        transaction.replace(R.id.frameContiner, homeFragment);
        transaction.addToBackStack(null);
        transaction.attach(homeFragment).commit();
    }
    public  void showAccount()
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        AccountFragment homeFragment=new AccountFragment();
        transaction.replace(R.id.frameContiner, homeFragment);
        transaction.addToBackStack(null);
        transaction.attach(homeFragment).commit();
    }

    public  void showFav()
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        FavouriteFragment homeFragment=new FavouriteFragment();
        transaction.replace(R.id.frameContiner, homeFragment);
        transaction.addToBackStack(null);
        transaction.attach(homeFragment).commit();
    }
    public  void showOrder()
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        OrderFragment homeFragment=new OrderFragment();
        transaction.replace(R.id.frameContiner, homeFragment);
        transaction.addToBackStack(null);
        transaction.attach(homeFragment).commit();
    }
    public  void showSchedule()
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        ScheduleFragment homeFragment=new ScheduleFragment();
        transaction.replace(R.id.frameContiner, homeFragment);
        transaction.addToBackStack(null);
        transaction.attach(homeFragment).commit();
    }


    public void onBackPressed() {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Exit Application?");
        alertDialogBuilder
                .setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                moveTaskToBack(true);
                                android.os.Process.killProcess(android.os.Process.myPid());
                                System.exit(1);
                            }
                        })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


}
