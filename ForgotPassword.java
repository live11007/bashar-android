package com.fora.foodapp.autho;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fora.foodapp.R;
import com.fora.foodapp.dashboard.DashboardMain;
import com.fora.foodapp.database.DatabaseAccess;
import com.fora.foodapp.database.User;
import com.fora.foodapp.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPassword extends AppCompatActivity {

    @BindView(R.id.txtLogin)TextView txtLogin;
    @BindView(R.id.btnSend)Button btnSend;
    @BindView(R.id.edtEmail)EditText edtEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

    }
    @OnClick({R.id.btnSend,R.id.txtLogin})
    public  void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnSend:

                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
                String storedPassword=databaseAccess.getForGotPasswrord(edtEmail.getText().toString());
                if(storedPassword==null)
                {
                    Toast.makeText(getApplicationContext(), "Please enter correct  email and phone number", Toast.LENGTH_SHORT).show();
                }else{
                    Log.d("GET PASSWORD",storedPassword);

                    Utils.showErrorMessage(getApplicationContext(),"Your password is: "+storedPassword);
                }
                break;

            case R.id.txtLogin:
                Intent intent = new Intent(ForgotPassword.this, LoginActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

                break;
        }
    }
}
